app.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

	var home = {
		name: "home",
		url: "/",
		templateUrl: "app/views/home/index.html",
		controller: "homeController"
	};

	var people = {
		name: "people",
		url: "/people/:personId",
		templateUrl: "app/views/home/show.html",
		controller: "peopleController",
	}

	var film = {
		name: "film",
		url: "/films/:filmId",
		templateUrl: "app/views/films/show.html",
		controller: "showFilmsController",
	}

	var vehicle = {
		name: "vehicle",
		url: "/vehicles/:vehicleId",
		templateUrl: "app/views/vehicles/show.html",
		controller: "showVehiclesController",
	}

	var starship = {
		name: "starship",
		url: "/starships/:starshipId",
		templateUrl: "app/views/starships/show.html",
		controller: "showStarshipsController",
	}

	$stateProvider.state(home);
	$stateProvider.state(people);
	$stateProvider.state(film);
	$stateProvider.state(vehicle);
	$stateProvider.state(starship);
});