app.controller('homeController', function($scope, $http) {
	$http.get("http://swapi.co/api/people/?page=1").then(function(response){
		$scope.people = response.data.results;
		$scope.loading = true;
		page = 1;

		$scope.loadMore = function () {
			$http.get("http://swapi.co/api/people/?page=" + (page + 1)).success(function(data){
				people = data.results;
				if(data.status!=404) {
					for(person in people) {
						$scope.people.push(people[person]);
					}
					page += 1;
					$scope.loading = false;
				}
			}).error(function(){
				$scope.loading = false;
			});
		};
	});
})
.controller("peopleController", function($scope, $stateParams, $http) {
	$scope.loading = true;
	$http.get("http://swapi.co/api/people/"+ $stateParams.personId).success(function(data){
		$scope.person = data;
		$scope.loading = false;

		$http.get(data.homeworld).success(function(data){
			$scope.person.homeworld = data;
		}).error(function(){
			$scope.person.homeworld = "unknown";
		});

		if($scope.person.films.length == 0) {
			$scope.person.films = "No Film";
		} else {
			films = [];

			for(film in data.films){
				$http.get(data.films[parseInt(film)]).success(function(data){
					data.filmId = data.url.replace("http://swapi.co/api/films/", "").replace("/", "");
					films.push(data);
				}).error(function(){
					$scope.person.films = [];
				});
			}
			$scope.person.films = films;
		}

		if($scope.person.vehicles.length == 0) {
			$scope.person.vehicles = [];
		} else {
			vehicles = [];

			for(vehicle in data.vehicles){
				$http.get(data.vehicles[parseInt(vehicle)]).success(function(data){
					data.vehicleId = data.url.replace("http://swapi.co/api/vehicles/", "").replace("/", "");
					vehicles.push(data);
				}).error(function(){
					$scope.person.vehicles = [];
				});
			}

			$scope.person.vehicles = vehicles;
		}

		if($scope.person.starships.length == 0) {
			$scope.person.starships = [];
		} else {
			starships = [];

			for(starship in data.starships){
				$http.get(data.starships[parseInt(starship)]).success(function(data){
					data.starshipId = data.url.replace("http://swapi.co/api/starships/", "").replace("/", "");
					starships.push(data);
				}).error(function(){
					$scope.person.starships = [];
				});
			}

			$scope.person.starships = starships;
		}
	}).error(function(){
		$scope.loading = false;
	});
})
.controller("showFilmsController", function($scope, $http, $stateParams) {
	$scope.loading = true;
	$http.get("http://swapi.co/api/films/" + $stateParams.filmId).success(function(data) {
		$scope.film = data;
		$scope.loading = false;
	});
})
.controller("showVehiclesController", function($scope, $http, $stateParams) {
	$scope.loading = true;
	$http.get("http://swapi.co/api/vehicles/" + $stateParams.vehicleId).success(function(data) {
		$scope.vehicle = data;
		$scope.loading = false;
	});
});